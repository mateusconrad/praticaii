package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.HorarioFuncionario;
import br.com.empresa.pratica2.repository.HorarioFuncionarioRepository;

@RestController
@RequestMapping("horariofuncionario")
public class HorarioFuncionarioResource {
    @Autowired
    private HorarioFuncionarioRepository horarioRepository;

    @GetMapping
    public List<HorarioFuncionario> findAll() {
        return horarioRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<HorarioFuncionario> salvar(@Valid @RequestBody HorarioFuncionario horario) {
        HorarioFuncionario horarioSalvo = horarioRepository.save(horario);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(horarioSalvo.getIdhorariofuncionario()).toUri();

        return ResponseEntity.created(location).body(horarioSalvo);
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<HorarioFuncionario> findByCodigo(@PathVariable Integer codigo) {
        try {
            HorarioFuncionario horario = horarioRepository.findById(codigo).get();

            return ResponseEntity.ok(horario);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        horarioRepository.deleteById(codigo);
    }
}