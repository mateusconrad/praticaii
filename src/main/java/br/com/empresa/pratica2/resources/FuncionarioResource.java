package br.com.empresa.pratica2.resources;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;
import javax.sql.DataSource;
import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import br.com.empresa.pratica2.model.Funcionario;
import br.com.empresa.pratica2.repository.FuncionarioRepository;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@RestController
@RequestMapping("funcionario")
public class FuncionarioResource {
    @Autowired
    private FuncionarioRepository funcionarioRepository;

    @Autowired
    private DataSource dataSource;

    @GetMapping("/report")
    public ResponseEntity<byte[]> relatorioFichaFuncional(@RequestParam("idfuncionario") int idfuncionario) {
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("idFuncionario", idfuncionario);

        byte[] reportBytes = null;

        try {
            InputStream io = this.getClass().getResourceAsStream("/reports/ficha_funcional_report.jasper");
            JasperPrint jasperPrint = JasperFillManager.fillReport(io, parametros, dataSource.getConnection());

            reportBytes = JasperExportManager.exportReportToPdf(jasperPrint);
        } catch (JRException | SQLException ex) {
            return ResponseEntity.noContent().build();
        }
        Funcionario funcionario = funcionarioRepository.findById(idfuncionario).get();

        return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment;filename=\"fichaFuncional_" + funcionario.getNomefuncionario() + ".pdf\"")
                .body(reportBytes);
    }

    @PostMapping
    public ResponseEntity<Funcionario> salvar(@Valid @RequestBody Funcionario funcionario) {
        Funcionario funcionarioSalvo = funcionarioRepository.save(funcionario);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(funcionarioSalvo.getIdfuncionario()).toUri();

        return ResponseEntity.created(location).body(funcionarioSalvo);
    }

    @GetMapping
    public List<Funcionario> findAll() {
        return funcionarioRepository.findAll();
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Funcionario> findByCodigo(@PathVariable Integer codigo) {
        try {
            Funcionario funcionario = funcionarioRepository.findById(codigo).get();

            return ResponseEntity.ok(funcionario);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        funcionarioRepository.deleteById(codigo);
    }

}