package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.HistoricoCargo;
import br.com.empresa.pratica2.repository.HistoricoCargoRepository;
@RestController
@RequestMapping("historicocargo")
public class HistoricoCargoResource {
    @Autowired
    private HistoricoCargoRepository histRepository;

    @GetMapping
    public List<HistoricoCargo> findAll() {
        return histRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<HistoricoCargo> salvar(@Valid @RequestBody HistoricoCargo hist) {
        HistoricoCargo histSalvo = histRepository.save(hist);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(histSalvo.getIdhistcargo()).toUri();

        return ResponseEntity.created(location).body(histSalvo);
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<HistoricoCargo> findByCodigo(@PathVariable Integer codigo) {
        try {
            HistoricoCargo hist = histRepository.findById(codigo).get();

            return ResponseEntity.ok(hist);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        histRepository.deleteById(codigo);
    }
}