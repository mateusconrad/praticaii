package br.com.empresa.pratica2.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tipovaga")
public class Tipovaga {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idtipovaga;
    @NotNull
    private String descricaotipovaga;

    public int getIdtipovaga() {
        return this.idtipovaga;
    }

    public void setIdtipovaga(int idtipovaga) {
        this.idtipovaga = idtipovaga;
    }

    public String getDescricaotipovaga() {
        return this.descricaotipovaga;
    }

    public void setDescricaotipovaga(String descricaotipovaga) {
        this.descricaotipovaga = descricaotipovaga;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Tipovaga)) {
            return false;
        }
        Tipovaga tipovaga = (Tipovaga) o;
        return idtipovaga == tipovaga.idtipovaga && Objects.equals(descricaotipovaga, tipovaga.descricaotipovaga);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idtipovaga, descricaotipovaga);
    }

}