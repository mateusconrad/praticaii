package br.com.empresa.pratica2.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "genero")
public class Genero {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idgenero;
    @NotNull
    private String descricaogenero;

    public int getIdgenero() {
        return this.idgenero;
    }

    public void setIdgenero(int idgenero) {
        this.idgenero = idgenero;
    }

    public String getDescricaogenero() {
        return this.descricaogenero;
    }

    public void setDescricaogenero(String descricaogenero) {
        this.descricaogenero = descricaogenero;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Genero)) {
            return false;
        }
        Genero genero = (Genero) o;
        return idgenero == genero.idgenero && Objects.equals(descricaogenero, genero.descricaogenero);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idgenero, descricaogenero);
    }
}