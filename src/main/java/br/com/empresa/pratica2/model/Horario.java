package br.com.empresa.pratica2.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "horario")
public class Horario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idhorario;
    private String descricaohorario;
    @NotNull
    private Date inimanha;
    @NotNull
    private Date fimmanha;
    @NotNull
    private Date initarde;
    @NotNull
    private Date fimtarde;

    public int getIdhorario() {
        return this.idhorario;
    }

    public void setIdhorario(int idhorario) {
        this.idhorario = idhorario;
    }

    public String getDescricaohorario() {
        return this.descricaohorario;
    }

    public void setDescricaohorario(String descricaohorario) {
        this.descricaohorario = descricaohorario;
    }

    public Date getInimanha() {
        return this.inimanha;
    }

    public void setInimanha(Date inimanha) {
        this.inimanha = inimanha;
    }

    public Date getFimmanha() {
        return this.fimmanha;
    }

    public void setFimmanha(Date fimmanha) {
        this.fimmanha = fimmanha;
    }

    public Date getInitarde() {
        return this.initarde;
    }

    public void setInitarde(Date initarde) {
        this.initarde = initarde;
    }

    public Date getFimtarde() {
        return this.fimtarde;
    }

    public void setFimtarde(Date fimtarde) {
        this.fimtarde = fimtarde;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Horario)) {
            return false;
        }
        Horario horario = (Horario) o;
        return idhorario == horario.idhorario && Objects.equals(descricaohorario, horario.descricaohorario)
                && Objects.equals(inimanha, horario.inimanha) && Objects.equals(fimmanha, horario.fimmanha)
                && Objects.equals(initarde, horario.initarde) && Objects.equals(fimtarde, horario.fimtarde);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idhorario, descricaohorario, inimanha, fimmanha, initarde, fimtarde);
    }
}