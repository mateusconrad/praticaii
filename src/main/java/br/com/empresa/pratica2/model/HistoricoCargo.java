package br.com.empresa.pratica2.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "historicocargo")
public class HistoricoCargo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idhistcargo;
    private Date datainicial;
    private Date datafinal;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "idfuncionario")
    private Funcionario idfuncionario;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "idcargo")
    private Cargo idcargo;

    public int getIdhistcargo() {
        return this.idhistcargo;
    }

    public void setIdhistcargo(int idhistcargo) {
        this.idhistcargo = idhistcargo;
    }

    public Date getDatainicial() {
        return this.datainicial;
    }

    public void setDatainicial(Date datainicial) {
        this.datainicial = datainicial;
    }

    public Date getDatafinal() {
        return this.datafinal;
    }

    public void setDatafinal(Date datafinal) {
        this.datafinal = datafinal;
    }

    public Funcionario getIdfuncionario() {
        return this.idfuncionario;
    }

    public void setIdfuncionario(Funcionario idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    public Cargo getIdcargo() {
        return this.idcargo;
    }

    public void setIdcargo(Cargo idcargo) {
        this.idcargo = idcargo;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof HistoricoCargo)) {
            return false;
        }
        HistoricoCargo historicoCargo = (HistoricoCargo) o;
        return idhistcargo == historicoCargo.idhistcargo && Objects.equals(datainicial, historicoCargo.datainicial)
                && Objects.equals(datafinal, historicoCargo.datafinal)
                && Objects.equals(idfuncionario, historicoCargo.idfuncionario)
                && Objects.equals(idcargo, historicoCargo.idcargo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idhistcargo, datainicial, datafinal, idfuncionario, idcargo);
    }
}