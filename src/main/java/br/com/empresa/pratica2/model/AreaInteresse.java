package br.com.empresa.pratica2.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "areainteresse")
public class AreaInteresse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idareainteresse;
    private String descricaointeresse;

    public void setIdareainteresse(int idareainteresse) {
        this.idareainteresse = idareainteresse;
    }

    public String getDescricaointeresse() {
        return this.descricaointeresse;
    }

    public void setDescricaointeresse(String descricaointeresse) {
        this.descricaointeresse = descricaointeresse;
    }

    public int getIdareainteresse() {
        return this.idareainteresse;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof AreaInteresse)) {
            return false;
        }
        AreaInteresse areaInteresse = (AreaInteresse) o;
        return idareainteresse == areaInteresse.idareainteresse
                && Objects.equals(descricaointeresse, areaInteresse.descricaointeresse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idareainteresse, descricaointeresse);
    }

}