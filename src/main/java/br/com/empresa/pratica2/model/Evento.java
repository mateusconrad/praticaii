package br.com.empresa.pratica2.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "evento")
public class Evento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idevento;
    @NotNull
    private String descricaoevento;
    @NotNull
    private String tipoevento;

    public int getIdevento() {
        return this.idevento;
    }

    public void setIdevento(int idevento) {
        this.idevento = idevento;
    }

    public String getDescricaoevento() {
        return this.descricaoevento;
    }

    public void setDescricaoevento(String descricaoevento) {
        this.descricaoevento = descricaoevento;
    }

    public String getTipoevento() {
        return this.tipoevento;
    }

    public void setTipoevento(String tipoevento) {
        this.tipoevento = tipoevento;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Evento)) {
            return false;
        }
        Evento evento = (Evento) o;
        return idevento == evento.idevento && Objects.equals(descricaoevento, evento.descricaoevento)
                && Objects.equals(tipoevento, evento.tipoevento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idevento, descricaoevento, tipoevento);
    }
}