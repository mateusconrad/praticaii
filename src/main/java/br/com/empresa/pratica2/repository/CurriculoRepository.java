package br.com.empresa.pratica2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import br.com.empresa.pratica2.model.Curriculo;

@Repository
public interface CurriculoRepository extends JpaRepository<Curriculo, Integer> {
    // o nome da tabela na sql precisa estar com a INICIAL EM MAIÚSCULO
    // (Curriculo, Pessoa, Vaga), pq ele referencia da MODEL

    @Query("select c.idcurriculo,c.idpessoa,coalesce(c.idvaga,0)as idvaga,coalesce(v.descricaovaga,'')as descricaovaga, "
            + " c.experienciascurriculo,c.habilidadescurriculo,p.nomepessoa,p.datanascimentopessoa,"
            + " g.idgenero,g.descricaogenero,e.idescolaridade,e.descricaoescolaridade,"
            + " cid.idcidade,cid.descricaocidade from Curriculo as c inner join Pessoa p on(p.idpessoa=c.idpessoa) "
            + " left outer join Vaga v on(v.idvaga=c.idvaga) inner join Genero g on(g.idgenero=p.idgenero)"
            + " inner join Escolaridade e on(e.idescolaridade=p.idescolaridade) "
            + " inner join Cidade cid on(cid.idcidade=p.idcidade) where upper(p.nomepessoa) like upper(?1)")
    public List<Curriculo> pesquisaPorNomepessoa(String nomepessoa);

    @Query("select c.idcurriculo,c.idpessoa,coalesce(c.idvaga,0)as idvaga,coalesce(v.descricaovaga,'')as descricaovaga, "
            + " c.experienciascurriculo,c.habilidadescurriculo,p.nomepessoa,p.datanascimentopessoa,"
            + " g.idgenero,g.descricaogenero,e.idescolaridade,e.descricaoescolaridade,"
            + " cid.idcidade,cid.descricaocidade from Curriculo as c inner join Pessoa p on(p.idpessoa=c.idpessoa) "
            + " left outer join Vaga v on(v.idvaga=c.idvaga) inner join Genero g on(g.idgenero=p.idgenero)"
            + " inner join Escolaridade e on(e.idescolaridade=p.idescolaridade) "
            + " inner join Cidade cid on(cid.idcidade=p.idcidade) where extract(year from age(p.datanascimentopessoa))=?1")
    public List<Curriculo> pesquisaPorIdade(int idade);

    @Query("select c.idcurriculo,c.idpessoa,coalesce(c.idvaga,0)as idvaga,coalesce(v.descricaovaga,'')as descricaovaga, "
            + " c.experienciascurriculo,c.habilidadescurriculo,p.nomepessoa,p.datanascimentopessoa,"
            + " g.idgenero,g.descricaogenero,e.idescolaridade,e.descricaoescolaridade,"
            + " cid.idcidade,cid.descricaocidade from Curriculo as c inner join Pessoa p on(p.idpessoa=c.idpessoa) "
            + " left outer join Vaga v on(v.idvaga=c.idvaga) inner join Genero g on(g.idgenero=p.idgenero)"
            + " inner join Escolaridade e on(e.idescolaridade=p.idescolaridade) "
            + " inner join Cidade cid on(cid.idcidade=p.idcidade) where g.idgenero=?1")
    public List<Curriculo> pesquisaPorSexo(int idgenero);

    @Query("select c.idcurriculo,c.idpessoa,coalesce(c.idvaga,0)as idvaga,coalesce(v.descricaovaga,'')as descricaovaga, "
            + " c.experienciascurriculo,c.habilidadescurriculo,p.nomepessoa,p.datanascimentopessoa,"
            + " g.idgenero,g.descricaogenero,e.idescolaridade,e.descricaoescolaridade,"
            + " cid.idcidade,cid.descricaocidade from Curriculo as c inner join Pessoa p on(p.idpessoa=c.idpessoa) "
            + " left outer join Vaga v on(v.idvaga=c.idvaga) inner join Genero g on(g.idgenero=p.idgenero)"
            + " inner join Escolaridade e on(e.idescolaridade=p.idescolaridade) "
            + " inner join Cidade cid on(cid.idcidade=p.idcidade) where e.idescolaridade=?1")
    public List<Curriculo> pesquisaPorEscolaridade(int idescolaridade);

    @Query("select c.idcurriculo,c.idpessoa,coalesce(c.idvaga,0)as idvaga,coalesce(v.descricaovaga,'')as descricaovaga, "
            + " c.experienciascurriculo,c.habilidadescurriculo,p.nomepessoa,p.datanascimentopessoa,"
            + " g.idgenero,g.descricaogenero,e.idescolaridade,e.descricaoescolaridade,"
            + " cid.idcidade,cid.descricaocidade from Curriculo as c inner join Pessoa p on(p.idpessoa=c.idpessoa) "
            + " left outer join Vaga v on(v.idvaga=c.idvaga) inner join Genero g on(g.idgenero=p.idgenero)"
            + " inner join Escolaridade e on(e.idescolaridade=p.idescolaridade) "
            + " inner join Cidade cid on(cid.idcidade=p.idcidade) where upper(cid.descricaocidade) like upper(?1)")
    public List<Curriculo> pesquisaPorCidade(String cidade);
}